﻿namespace TestProxy
{
    public class Startup
    {
        public void ConfigureServices(IServiceCollection services)
        {
        }

        public void Configure(IApplicationBuilder app, Microsoft.AspNetCore.Hosting.IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseMiddleware<TestProxyMiddleware>();

            app.Run(async (context) =>
            {
                await context.Response.WriteAsync($"<a href='/reddit/r/popular/'>Reddit</a>");
            });
        }
    }
}